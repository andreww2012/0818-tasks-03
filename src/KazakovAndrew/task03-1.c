﻿// Написать программу, принимающую от пользователя несколько строк. Строки вводятся либо до достижения максимального количества (N) либо до ввода пустой строки.
// Вывести строки на экран в обратном порядке.
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <windows.h>
#define STR_AMOUNT 7
#define STR_LENGTH 70

int main() {

	int i;
	int stop_str;
	char str[STR_AMOUNT][STR_LENGTH];

	setlocale(LC_ALL, "rus");
	SetConsoleCP(1251); // установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251); // установка кодовой страницы win-cp 1251 в поток вывода

	i = 0;
	stop_str = STR_AMOUNT; // Строка, на которой закончен ввод данных
	do {

		printf("Введите строку № %d: ", i + 1);
		fgets(str[i], 70, stdin);
		str[i][strlen(str[i]) - 1] = 0;

		if (*str[i] == 0) { // Таким образом останавливаем ввод строк при вводе пустой строки
			stop_str = i;
			i = STR_AMOUNT;
		}

	} while (++i < STR_AMOUNT);

	if (stop_str == 0) {
		puts("Вы не ввели ни одной непустой строки.");
	}
	else {

		puts("Те же строки в обратном порядке:");

		for (i = stop_str - 1; i >= 0; i--) {
			printf("%s \n", str[i]);
		}

	}

	return 0;
}