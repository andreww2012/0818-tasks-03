﻿// Написать программу, принимающую от пользователя несколько строк. Строки вводятся либо до достижения максимального количества (N) либо до ввода пустой строки.
// Вывести строки на экран в порядке возрастания их длин.
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <windows.h>
#define STR_AMOUNT 7
#define STR_LENGTH 70

int compare(const void *a, const void *b) {
	return strlen(*(char **)a) > strlen(*(char **)b);
}

int main() {

	int i;
	int stop_str;
	char **str;

	setlocale(LC_ALL, "rus");
	SetConsoleCP(1251); // установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251); // установка кодовой страницы win-cp 1251 в поток вывода

	// Создаём массив
	str = (char**)malloc(STR_AMOUNT*sizeof(char*));
	for (i = 0; i < STR_AMOUNT; i++) {
		str[i] = (char*)malloc(STR_LENGTH*sizeof(char));
	}

	i = 0;
	stop_str = STR_AMOUNT; // Строка, на которой закончен ввод данных
	do {

		printf("Введите строку № %d: ", i + 1);
		fgets(str[i], 70, stdin);
		str[i][strlen(str[i]) - 1] = 0;

		if (*str[i] == 0) { // Таким образом останавливаем ввод строк при вводе пустой строки
			stop_str = i;
			i = STR_AMOUNT;
		}

	} while (++i < STR_AMOUNT);

	if (stop_str == 0) {
		puts("Вы не ввели ни одной непустой строки.");
	}
	else {

		qsort(str, stop_str, sizeof(char*), compare);

		puts("Строки, отсортированные по возрастанию их длин:");

		for (i = 0; i < stop_str; i++) {
			printf("%s\n", str[i]);
		}

	}

	return 0;
}